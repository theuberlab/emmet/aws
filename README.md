# AWS

Some Notes and links on AWS training.

# Free Online Training from AWS
AWS has some great [free online learning available here](https://explore.skillbuilder.aws/learn)

I recommend starting in this order
1. [Cloud Foundations](https://explore.skillbuilder.aws/learn/public/learning_plan/view/82/cloud-foundations-learning-plan)
1. [Devops Engineer](https://explore.skillbuilder.aws/learn/public/learning_plan/view/85/devops-engineer-learning-plan)
1. [Containers](https://explore.skillbuilder.aws/learn/public/learning_plan/view/83/containers-learning-plan)
1. [Architect](https://explore.skillbuilder.aws/learn/public/learning_plan/view/78/architect-learning-plan)


# Resources
[AWS Deep Dive Videos](https://www.youtube.com/c/amazonwebservices/search) Youtube videos where they dig into specific AWS technologies.




# Skills to Develop for the Support Gig

terminal usage, incident response, documentation, ability to search slack/man/google/etc and find relevant information

Soft Skills: Patience, empathy, and quick thinking.
General knowledge of networking, Docker, Linux, and git.


## Above Turned Into Practicable Tasks

* Troubleshooting
    * Let's get together and I'll give you some troubleshooting scenarios.
* Emphasize your ability to learn quickly.
* Have some knowledge of docker
    * Be able to explain the difference between containers and virtualization
    * Familiarize self with docker commands ps, info, run
    * Know how to build a docker container
* Have some basic networking knowledge
    * What's a packet
    * What is TCP
    * What is IP
    * Know the OSI model
    * Know that it's different from the TCP model
    * Be able to explain what a subnet is
    * Be able to define CIDR (don't worry too hard about being able to do it off the top of your head.)

